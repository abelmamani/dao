from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, create_engine, MetaData, Table, select
from config_vars import BBDD_CONNECTION

Base = declarative_base()


class Provinces(Base):
    __tablename__ = "provinces"
    print("entering parameters config")
    engine = create_engine(BBDD_CONNECTION)
    metadata = MetaData()
    pro = Table("provinces", metadata, autoload=True, autoload_with=engine, schema='billetera_abel')
    id_not_in_db = Column(Integer, primary_key=True)
    print("finished config for parameters")
    
    @classmethod
    def single_porvinces(cls, *, pro_id):
        """
        cual es la provincia con id 
        """
        query = select([cls.pro]).where(cls.pro.c.pro_id == pro_id)
        return query
        
    @classmethod
    def all_provinces(cls):
        """
        Cuáles son todas las provincias
        """
        query = select([cls.pro])
        return query